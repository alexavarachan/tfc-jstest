var express = require('express');
var app = express();
var crypto = require('crypto');
var path = require('path');
var counter=0;
app.use(express.static('../client'));


var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: false });
app.use(bodyParser.json()); 

app.get('/jstest',urlencodedParser, function (req, res) {
   res.sendFile( path.join(__dirname, '../client', 'index.htm') );
})


app.post('/api/hash',function (req, res) {

   response = {
   		submitNumberofTask : req.body.submitNumberofTask,
		optionToSubmit : req.body.optionToSubmit,
		valueToSubmit : req.body.valueToSubmit,
		result : crypto.createHash('md5').update(req.body.valueToSubmit).digest("hex")
   };
   res.contentType('application/json');
   res.json(response);
})

app.post('/api/counter', function (req, res) {

   var result;
   if(req.body.valueToSubmit <= 0)
   		result = "INVALID VALUE!";
   	else
   		result = parseInt(req.body.valueToSubmit)+1;

   response = {
   		submitNumberofTask : req.body.submitNumberofTask,
		optionToSubmit : req.body.optionToSubmit,
		valueToSubmit : req.body.valueToSubmit,
		result : result
   };
   res.contentType('application/json');
   res.end(JSON.stringify(response));
})

app.post('/api/global-counter', function (req, res) {

   response = {
   		submitNumberofTask : req.body.submitNumberofTask,
		optionToSubmit : req.body.optionToSubmit,
		valueToSubmit : req.body.valueToSubmit,
		valuePrevious : counter,
		result : parseInt(req.body.valueToSubmit)+counter,
   };
   counter = parseInt(req.body.valueToSubmit);
   res.contentType('application/json');
   res.end(JSON.stringify(response));
})




var server = app.listen(8082, function () {

  var host = server.address().address
  var port = server.address().port

  console.log("Example app listening at http://%s:%s", host, port)

})
