//this is myjs
var dataAjax={};
var submitNumber=1;
function submitValue(){
	
	dataAjax = {
		submitNumberofTask :submitNumber,
		optionToSubmit : document.getElementById("optionToSubmit").value,
		valueToSubmit : document.getElementById("valueToSubmit").value
	};
	if(dataAjax.optionToSubmit == "counter" || dataAjax.optionToSubmit == "global-counter"){
		if(!(Number(parseInt(dataAjax.valueToSubmit)) === parseInt(dataAjax.valueToSubmit) && parseInt(dataAjax.valueToSubmit) % 1 === 0)){
			alert("Please enter an integer");
			return false;	
		}
	}
	$.ajax({
		 data: JSON.stringify(dataAjax), 
		 type: "POST",
		 dataType:'json',
	     url: "http://127.0.0.1:8082/api/"+dataAjax.optionToSubmit,
	     contentType:'application/json',
	     success: function(dat) {
		   console.log(dat);
		   var htmlToWrite = "<br><div>"+dat.optionToSubmit+"(#"+dat.submitNumberofTask+")</div>" 
		        		    +"<table style = 'width:300px;border:1px;'><tr><th>Input</th>"
		   if(dat.optionToSubmit == "global-counter"){
		   		htmlToWrite +="<th>Old Counter</th>"
		   }
		   htmlToWrite +="<th>Output</th></tr>"
		        		+"<tr><td>"+dat.valueToSubmit+"</td>"
		   if(dat.optionToSubmit == "global-counter"){
		   		htmlToWrite +="<td>"+dat.valuePrevious+"</td>"
		   }
		   htmlToWrite	+= "<td>"+dat.result+"</td></tr></table>";
		   $('#results').prepend(htmlToWrite);
		 }
	});
		
	submitNumber++;
    
};
